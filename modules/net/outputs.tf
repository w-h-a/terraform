output "vpc" {
    value = aws_vpc.myapp_vpc
}

output "subnet" {
    value = aws_subnet.myapp_subnet_1
}
