terraform {
    required_version = ">= 0.12"
    backend "s3" {
        bucket = "w-h-a-terraform-bucket"
        key = "myapp/state.tfstate"
        region = "us-east-1"
    }
}

provider "aws" {
    region = "us-east-1"
}

module "myapp_net" {
    source = "./modules/net"
    vpc_cidr_block = var.vpc_cidr_block
    subnet_cidr_block = var.subnet_cidr_block
    avail_zone = var.avail_zone
    env_prefix = var.env_prefix
}

module "myapp_webserver" {
    source = "./modules/webserver"
    instance_type = var.instance_type
    public_key_path = var.public_key_path
    ssh_key_private = var.ssh_key_private
    avail_zone = var.avail_zone
    env_prefix = var.env_prefix
    my_ip = var.my_ip
    image_name = var.image_name
    subnet_id = module.myapp_net.subnet.id
    vpc_id = module.myapp_net.vpc.id
}



